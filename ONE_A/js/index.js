//
// (function() {
//   var population = new Population();
//   if (JSON.parse(localStorage.getItem("population"))) {
//       population.addPopulationToView();
//   }
//   // call method to start rate refresh
//   population.callSelf();
// })();
//
// function Population(){
//
//     this.saveItem = function(data, objectName) {
//         if (typeof(Storage) !== "undefined") {
//             var arr = [];
//             // check prev data
//             // check existence of id or gen new one
//             var prevData = JSON.parse(localStorage.getItem(objectName));
//             if (prevData !== null) {
//                 prevData.map(function(el) {
//                   arr.push(el);
//                 });
//                 arr.push(data);
//                 localStorage.setItem(objectName,JSON.stringify(arr));
//             } else {
//                 localStorage.setItem(objectName,JSON.stringify(arr));
//                 arr.push(data)
//                 localStorage.setItem(objectName, JSON.stringify(arr));
//             }
//         } else {
//             // Sorry! No Web Storage support..
//             console.log("Error: you don't have localStorage!");
//         }
//     };
//
//     this.callSelf = function() {
//       setTimeout(function() {
//         this.addPopulationToView();
//         this.callSelf();
//       }, 1000);
//     };
//
//     this.addPopulationToView = function() {
//       $('#country').empty();
//       JSON.parse(localStorage.getItem("population")).map(function(pop) {
//         var $li = '<li>'+ pop.country + ' - ' + pop.today +'<span><i id="item_' + pop.id + '"';
//         // check if population is increasing and
//         // show appropriate font awesome icon
//         var status = this.countRate(this.rateCallback(pop), pop);
//         if (status === "increasing") {
//           $li += ' class="fa fa-angle-up" style="padding-left:10px;font-size:20px;color:blue"';
//           $li += ' aria-hidden="true"></i>';
//         } else if (status === "decreasing") {
//           $li += ' class="fa fa-angle-down" style="padding-left:10px;font-size:20px;color:red"';
//           $li += ' aria-hidden="true"></i>';
//         } else {
//           // $li += ' style="padding-left:10px;font-size:20px;"';
//           $li += '-';
//         }
//
//         $li += '</span></li>';
//         $('#country').append($li);
//       });
//     };
//
//     this.rateCallback = function() {
//         var population = JSON.parse(localStorage.getItem("population"));
//         for (var i = 0; i < population.length; i++) {
//           if (population[i].id === pop.id) {
//             return pop.id;
//           }
//         }
//         return -1;
//     };
//
//     this.addCountry = function() {
//         // Get entrered and
//         // Capitalize country first letter
//         var country = ($('#input').val())[0].toUpperCase() + ($('#input').val()).slice(1);
//
//         if (country) {
//             var url = "http://api.population.io/1.0/population/";
//             url += country + "/today-and-tomorrow";
//             // download country populatio data from API
//             $.ajax({
//                 url: url,
//                 beforeSend: function(xhr) {
//                   xhr.overrideMimeType("text/plain; charset=x-user-defined");
//                 }
//             }).done(function(data) {
//                 if (data) {
//                     var population = JSON.parse(data);
//                     var today = population.total_population[0]['population'];
//                     var tomorrow = population.total_population[1]['population'];
//
//                     if (Storage) {
//                         var population = JSON.parse(localStorage.getItem("population"));
//                         var count = 0;
//                         if (population) {
//                           for (var i = 0; i < population.length; i++) {
//                             count = (population.length) + 1;
//                           }
//                           this.saveItem({
//                             id: count,
//                             country: country,
//                             today: today,
//                             tomorrow: tomorrow
//                           }, "population");
//                         } else {
//                           this.saveItem({
//                             id: count,
//                             country: country,
//                             today: today,
//                             tomorrow: tomorrow
//                           }, "population");
//                         }
//                         this.addPopulationToView();
//                     }
//                 }
//             }).fail(function(error) {
//                 alert("Error adding "+ country + ", Please check spelling!!");
//             });
//         }
//         $('#input').val("");
//     };
//
//     this.searchCountryPopulation = function(country) {
//         console.log(country);
//     };
//
//     this.countRate = function(callback, country) {
//         var ratePerSec = 0;
//         var initDifference = 0
//         if (country.today && country.tomorrow) {
//           // calculate population difference
//           initDifference = country.tomorrow - country.today;
//           ratePerSec = (initDifference) / (24 * 60 * 60);
//           var arr = [];
//           // check prev data
//           // check existence of id
//           var local = JSON.parse(localStorage.getItem("population"));
//           if (local !== null) {
//               local.map(function(el) {
//                 if (callback !== el.id) {
//                   arr.push(el);
//                 }
//               });
//               arr.push({
//                 id: country.id,
//                 country: country.country,
//                 today: country.today + Math.floor((ratePerSec * 100)),
//                 tomorrow: country.tomorrow
//               });
//               localStorage.setItem("population",JSON.stringify(arr));
//           }
//           // check if population is increasing or decreasing
//           var todayAfterRate = country.today + Math.floor(ratePerSec * 100);
//           if (country.today > todayAfterRate ) {
//             return "decreasing";
//           } else if (country.today === todayAfterRate) {
//             return "stable";
//           } else {
//             return "increasing";
//           }
//         }
//     };
// }
