
(function() {
  populateRoomsToView($('#list'));
})();

/* Using JQuery */
function removeItem(id) {
  if (id) {
    if (typeof(Storage) !== "undefined") {
        var arr = [];
        // check prev data
        // check existence of id
        var local = JSON.parse(localStorage.getItem("items"));
        if (local !== null) {
            local.map(function(el) {
              // console.log(id.slice(5, id.length));parseInt(id.slice(5, id.length))
              if (id !== el.id) {
                arr.push(el);
              }
            });
            localStorage.setItem("items",JSON.stringify(arr));
            addItemsToView($('#items'));
        }
    } else {
        // Sorry! No Web Storage support..
        console.log("Error: you don't have localStorage!");
    }
  }
};

function searchRoom(value) {
  if (value) {
      var filter = value.toUpperCase();
      var ul = document.getElementById("list");
      var li = ul.getElementsByTagName('li');
      displaySearchResult(li, value);
  } else {
      populateRoomsToView($('#list'));
  }
}

function populateRoomsToView(ul) {
    // clear view
    ul.empty();

    var rooms = ['Euler', 'Java', 'C', 'Lisp', 'Simula', 'Smalltalk', 'Shell', 'Scheme'];
    applyToElements((room) => {
      var $li = '<li>'+ room + '</li>'
      ul.append($li);
    }, rooms);
}

function applyToElements(callback, list) {
    var result = [];
    for (var i = 0; i < list.length; i++) {
      result.push(callback(list[i]));
    }
    return result;
}

function elementSearchWord(element, searchWord) {
    if ((element.toUpperCase()).startsWith(searchWord.toUpperCase())) {
        return true;
    } else {
        return false;
    }
}

function displaySearchResult(list, searchWord) {
    for (var i = 0; i < list.length; i++) {
        if (elementSearchWord(list[i].innerHTML, searchWord)) {
            list[i].style.display = "";
        } else {
            list[i].style.display = "none";
        }
    }
}
