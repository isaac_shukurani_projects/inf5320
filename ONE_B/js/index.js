
(function() {
  init();
})();

/* Using JQuery */
function removeItem(id) {
  if (id) {
    if (typeof(Storage) !== "undefined") {
        var arr = [];
        // check prev data
        // check existence of id
        var local = JSON.parse(localStorage.getItem("items"));
        if (local !== null) {
            local.map(function(el) {
              // console.log(id.slice(5, id.length));parseInt(id.slice(5, id.length))
              if (id !== el.id) {
                arr.push(el);
              }
            });
            localStorage.setItem("items",JSON.stringify(arr));
            addItemsToView($('#items'));
        }
    } else {
        // Sorry! No Web Storage support..
        console.log("Error: you don't have localStorage!");
    }
  }
};

function addItemsToView(ul) {
    if (Storage) {
      // clear view
      ul.empty();
      // load from local storage to view
      JSON.parse(localStorage.getItem("items")).map(function(myitem) {
        if (myitem !== null) {
           var $li = '<li id="'+ myitem.id
            +'" onClick="removeItem('+ myitem.id+')">'+myitem.item+'<span><i'
            +' class="fa fa-trash-alt text-danger" aria-hidden="true" style="color:red;"></i></span></li>';
            ul.append($li);
          $('#input').val("");
        }
      });
    }
}

function init() {
    var $ul = $('#items');
    var count = 0;
    $('#add').click(function() {
      var val = $('#input').val();
      // console.log(val);
      if (val) {
        count++;
        var local = JSON.parse(localStorage.getItem("items"));
        if (local) {
          local.map(function(it) {
            if (it.id === count) {
              count++;
            }
          });
        }
        saveItem({id:count, item:val}, "items");
        addItemsToView($ul);
      }
    });
    addItemsToView($ul);
}

function saveItem(data, objectName) {
    if (typeof(Storage) !== "undefined") {
        var arr = [];
        // check prev data
        // check existence of id or gen new one
        var prevData = JSON.parse(localStorage.getItem(objectName));
        if (prevData !== null) {
            prevData.map(function(el) {
              arr.push(el);
            });
            arr.push(data);
            localStorage.setItem(objectName,JSON.stringify(arr));
        } else {
            localStorage.setItem(objectName,JSON.stringify(arr));
            arr.push(data)
            localStorage.setItem(objectName, JSON.stringify(arr));
        }
    } else {
        // Sorry! No Web Storage support..
        console.log("Error: you don't have localStorage!");
    }
}

function searchItem(value) {
  if (value) {
    if (Storage) {
      // load from local storage to view
      JSON.parse(localStorage.getItem("items")).map(function(myitem) {
        var lower = (myitem.item).toLowerCase();
        if (lower.startsWith(value.toLowerCase())) {
          // clear view
          $('#items').empty();
          var $li = '<li>'+ myitem.item
            + '<span><i id="'+ myitem.id
            +'" onClick="removeItem('+ myitem.id +')"'
            +' class="fa fa-trash-alt text-danger" aria-hidden="true" style="color:red;"></i></span></li>'
          $('#items').append($li);
        }
      });
    }
  } else {
    addItemsToView($('#items'));
  }
}
